const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

var express = require("express");
var app = express();
var bodyParser = require('body-parser');
//var request = require('request');
// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/firestore");
require("firebase/database");

var firebaseConfig = {
     apiKey: "AIzaSyAoUHWJHyfNaI0tffAIP4zvQf6Ahn5KWKw",
     authDomain: "dynamicalgonode.firebaseapp.com",
     databaseURL: "https://dynamicalgonode.firebaseio.com",
     projectId: "dynamicalgonode",
     storageBucket: "dynamicalgonode.appspot.com",
     messagingSenderId: "344932076367",
     appId: "1:344932076367:web:13f53ee2844fd0c1"
 };
 
 // Initialize Firebase
 firebase.initializeApp(firebaseConfig);

// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.use(bodyParser.json())
app.use(express.static('public'));

app.get('/', function(req, res){
   console.log("GET request from homepage");
   res.sendFile(__dirname+"/"+"Home.html");
})

// app.get('/', function(req, res){
//    res.send("Welcome");
// })

app.get('/CreateUser', function(req, res){
   console.log("GET request from CreateUser");
   res.sendFile(__dirname+"/"+"CreateUser.html");
})

app.post('/CreateUser',  urlencodedParser, function(req, res){
   //prepare output in JSON format
   console.log("POST request from CreateUser");
   User_data = {
      Name: req.body.Name,
      Email: req.body.Email,
      About: req.body.About,
      NID: req.body.NID
   };
   console.log(User_data);

   var name= req.body.Name;
   var email= req.body.Email;
   var about= req.body.About;
   var nid= req.body.NID
   var txt = "";
   writeUserData(name, email, about, nid).then(function(value){
      txt=value;
      if(txt == "ok"){
         res.sendFile(__dirname+"/"+"Success.html")
      }
      if(txt == "exist"){
         res.sendFile(__dirname+"/"+"Exist.html")
      }
      if(txt.length == 0 || txt == ""){
         res.sendFile(__dirname+"/"+"Failure.html")
      }
   })
})

app.post('/api/CreateUser', function(req, res){
   console.log("api POST request from CreateUser");
   User_data = {
      Name: req.body.Name,
      Email: req.body.Email,
      About: req.body.About,
      NID: req.body.NID
   };
   var name= req.body.Name;
   var email= req.body.Email;
   var about= req.body.About;
   var nid= req.body.NID
   var txt = "";
   writeUserData(name, email, about, nid).then(function(value){
      console.log("value: ",value);
      txt=value;
      console.log("txt: ", txt)

      if(txt=="ok"){
         res.status(201);
         res.end(JSON.stringify(User_data));
      }
      if(txt=="exist"){
         res.status(409);
         res.end(JSON.stringify("NID already exist"));
      }
      if(txt.length == 0 || txt == ""){
         res.status(400);
         res.end();
      }
   })
})

async function writeUserData(name, email, about, nid) {
   msg = "";
   var db = firebase.database();

   await db.ref().child("users").orderByChild("nid").equalTo(nid).once("value",snapshot => {
      if (snapshot.exists()){
         const userData = snapshot.val();
         console.log("NID already exists!\n");
         msg = "exist";
      }  
      else{
         console.log("Ready to create new user.");
         var newPostKey = firebase.database().ref().child('users').push().key;
         firebase.database().ref('users/' + newPostKey).set({
            username: name,
            email: email,
            about: about,
            uid: newPostKey,
            nid: nid
         }) 
         console.log("Saved"); msg = "ok";
      }
   })
   console.log("finally msg: "+msg);
   return msg;
 }

app.post('/isEmailExist', function(req, res){
   console.log("checking....");
   var userEmail = req.body.Email;
   console.log("user email: ", userEmail)
   msg = "";
   var db = firebase.database();
   db.ref().child("users").orderByChild("email").equalTo(userEmail).once("value",snapshot => {
      if (snapshot.exists()){
         const userData = snapshot.val();
         console.log("Email already exists!\n");
         msg = "Email already exists!";
         res.json(msg);
      }
      else{
         res.json(null);
      }
   })
})

app.post('/isNidExist', function(req, res){
   console.log("checking....");
   var userNid = req.body.NID;
   msg = "";
   var db = firebase.database();
   db.ref().child("users").orderByChild("nid").equalTo(userNid).once("value",snapshot => {
      if (snapshot.exists()){
         const userData = snapshot.val();
         console.log("NID already exists!\n");
         msg = "NID already exists!";
         res.json(msg);
      }
      else{
         res.json(null);
      }
   })
})

app.get('/GetUserInfo', function(req, res){
   console.log("GET request from GetUserInfo");
   res.sendFile(__dirname+"/"+"GetUserInfo.html");
})

app.post('/GetUserInfo', urlencodedParser, function(req, res){
   console.log("POST request from GetUserInfo");
   console.log("Query: "+JSON.stringify(req.body).trim());
   var nid = req.body.NID;
   console.log("NID: "+nid+", type: "+typeof(nid));

   var db = firebase.database();
   db.ref().child("users").orderByChild("nid").equalTo(nid).once("value",snapshot => {
      if (snapshot.exists()){
        const userData = snapshot.val();
        console.log("User data:\n", userData);
        var userId = Object.keys(userData);
        //console.log("user data: ", userData[userId]);
        //console.log("user name: ", userData[userId].username);
        var about = userData[userId].about;
        // working with python-shell module
        var ps = require('python-shell');
        var options = {
         mode: 'text',
         pythonPath: 'python',
         pythonOptions: ['-u'],
         //scriptPath: './start.py',
         args: [about]
        };

        ps.PythonShell.run('./start.py', options, function (err, results) {
         if (err) throw err;
         //results is an array consisting of messages collected during execution
         console.log('results: %j', results);
         user_data = {
            username: userData[userId].username,
            email: userData[userId].email,
            result: results.toString()
         };
            res.json(user_data);
     });
      }
   })
});

app.post('/api/GetUserInfo', function(req, res){
   console.log("api POST request from GetUserInfo");
   console.log("Query: "+req.query);
   var nid = req.query.NID;
   console.log("NID: "+nid+", type: "+typeof(nid));

   var db = firebase.database();
   db.ref().child("users").orderByChild("nid").equalTo(nid).once("value",snapshot => {
      if (snapshot.exists()){
        const userData = snapshot.val();
        console.log("User data:\n", userData);
        var userId = Object.keys(userData);
        //console.log("user data: ", userData[userId]);
        //console.log("user name: ", userData[userId].username);
        var about = userData[userId].about;
        
        //working with child_process module
        var spawn = require("child_process").spawn; 
        var process = spawn( 'python', ['./start.py', about] );
        process.stdout.on('data', function(data) { 
         console.log(data.toString())
         user_data = {
            username: userData[userId].username,
            email: userData[userId].email,
            result: data.toString()
         };
           res.json(user_data);
        })
      }
   })
});

app.get('/GetUserList', function(req, res){
   console.log("GET request from GetUserList");
   res.sendFile(__dirname+"/"+"GetUserList.html");
})

app.post('/GetUserList', urlencodedParser, function(req, res){
   console.log("POST request from GetUserList");
   var word = req.body.Keyword;

   var db = firebase.database();
   var userRef = db.ref().child('users');
   userRef.on('value', function(snapshot){
      var userList = JSON.stringify(snapshot.val());
      var group = JSON.parse(userList);
      var people = Object.keys(group);

      var obj = [];
      people.forEach(function(person){
         var items = Object.keys(group[person]);

         items.forEach(function(item){
            var value = group[person][item];
            //console.log(person+': '+item+"= "+value);
            //TODO: check only 'about' item
            var selectedItem = item;
            if(selectedItem == "about")
            {
               if(value.toLowerCase().includes(word.toLowerCase()))
               {
                  obj.push(group[person]);
               }
            }
         });
      });    
      res.json(obj);
   })
});

app.post('/api/GetList', function(req, res){
   console.log("api GET request");
   console.log("Query: "+req.query);
   var word = req.query.Keyword;
   console.log("keyword: "+word);

   var db = firebase.database();
   var userRef = db.ref().child('users');
   userRef.on('value', function(snapshot){
      var userList = JSON.stringify(snapshot.val());
      var group = JSON.parse(userList);
      var people = Object.keys(group);
      var obj = [];

      people.forEach(function(person){
         var items = Object.keys(group[person]);

         items.forEach(function(item){
            var value = group[person][item];
            //console.log(person+': '+item+"= "+value);
            //TODO: check only 'about' item
            var selectedItem = item;
            if(selectedItem == "about")
            {
               if(value.toLowerCase().includes(word.toLowerCase()))
               {
                  obj.push(group[person]);
               }
            }
         });
      });    
      res.json(obj);
   })
 })

 app.get('/Profile', function(req, res){
      console.log("GET request from profile");
      res.sendFile(__dirname+"/"+"Profile.html");
   })

 app.post('/Pro', urlencodedParser, function(req, res){
   console.log("Get request from profile");
   var userId = req.body.userID;
   console.log("user id: "+userId);

   var db = firebase.database();
   db.ref().child("users").orderByChild("uid").equalTo(userId).once("value",snapshot => {
      if (snapshot.exists()){
        const userData = snapshot.val();
        console.log("user data: ", userData[userId]);
        res.json(userData[userId]);
      }
   })
})

exports.app = functions.https.onRequest(app);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Our app is running on port ${ PORT }`);
});
