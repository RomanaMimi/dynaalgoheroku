import sys
import json
#import requests #not necessary
import string
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize 
from nltk.corpus import stopwords 
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer

#print("Output from Python: ")

def data_tokenize(p):   # token wise spreate
    pList = word_tokenize(p)
    # print("pList: ", pList)
    # print(" ")
    return remove_punc(pList)

def remove_punc(pList):
        list_of_punc = string.punctuation
        punc_remove = []
        for word in pList:
            if word not in list_of_punc:
                punc_remove.append(word)
        # print("punc_remove: ",punc_remove)
        # print(" ")
        return stop_words(punc_remove)

def stop_words(punc_remove):# stop_words
    stop_word = set(stopwords.words("english"))
    filter_sentence = []
    for w in punc_remove:
        if w not in stop_word:
            filter_sentence.append(w)
    filter_sentence_tag = nltk.pos_tag(filter_sentence)
    # print("filter List: ", filter_sentence)
    # print("filter List with tagg: ", filter_sentence_tag)
    # print(" ")
    return LemmaTize(filter_sentence)

def LemmaTize(filter_sentence):
    Limmer_Sentence = []
    lemmatizer = WordNetLemmatizer()
    for words in filter_sentence:
        Limmer_Sentence.append(lemmatizer.lemmatize(words))
    # print("Limmer", Limmer_Sentence)
    # print(nltk.pos_tag(Limmer_Sentence))
    # print()
    return Stemmer(Limmer_Sentence)

def Stemmer(Limmer_Sentence):
    porter = PorterStemmer()
    stems = []
    for t in Limmer_Sentence:
        stems.append(porter.stem(t))
    stems=list(dict.fromkeys(stems))
    # print("stemmer: ", stems)
    # print(nltk.pos_tag(stems))
    # print(" ")
    return result(stems)

def result(stems):
    count=int(0)
    catagory=['good','tax','vote','obid','help','educ','punctual','contribut','duti','patriot','courtesi','respect','worthi','honest','toler','account','moral','courag','respons','disciplin','care', 'friendli', 'passion', 'help', 'support', 'challeng', 'experi', 'plenti', 'loyal', 'talent', 'success', 'creativ', 'leader', 'innov', 'highli']
    for word in stems:
        if word in catagory:
            count=count+1
    goodness=round(((count/36)*100),2)
    return goodness
    #print(goodness,"%")

p = str(sys.argv[1])
#p = str("A business graduate student from University of X, looking to secure a Business Analyst position to utilise my current analytical skills and knowledge.")
p = p.lower()
# print(p)
# print(" ")
#data_tokenize(p)

# print("Output from Python\n")
print(data_tokenize(p),"%")

